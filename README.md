# O3b-RIFT-Rerun-Review

* Files used for the reruns of some O3b events. Regular name of files is the rerun; files ending in postfix "_orig" is the original run file. Example: ILE.sub is the rerun file, and ILE_orig.sub is from the original run.

## Events
* All directories are on CIT unless otherwise specified

### GW200322:
* Original run: /home/daniel.williams/events/O3/o3b/run_directories/GW200322/Prod11
* Rerun run:/home/jacob.lange/unixhome/Projects/O3b-analysis/GW200322/GW200322-BW-PSD-SEOBNRv4PHM-O3b-C01-60Hz-queue-15-neff-3-more-samples-Lmax4-fmin-20/

### GW200208a:
* Original run: /home/daniel.williams/events/O3/o3b/run_directories/GW200208_222617/Prod13/
* Rerun run:/home/jacob.lange/unixhome/Projects/O3b-analysis/S200208a/S200208a-O3b-rerun-for-lnL-tail-neff-15-queue-3-Lmax4-fmin-20-extrinsic/

### GW200208_13:
* Original run: /home/anjali.yelikar/rapid_pe_nr_review_o3/O3/Asimov/S200208a/G363349_C01_nonlinear_SEOBNRv4PHM_fmin20_fmin-template10.0_lmax4_default_gp_Bilbytime_v1/
* Rerun run:

### GW200224a:
* Original run: /home/daniel.williams/events/O3/o3b/run_directories/S200224a/Prod6
* Rerun run: /home/hectorluis.iglesias/projects/o3b/S200224a/S200224a-O3b-rerun-for-lnL-tail-Lmax4-fmin-20-extrinsic_neff_3_more_samples_with_GPU

### GW191216:
* Original run: /home/daniel.williams/events/O3/o3b/run_directories/S191216a/Prod11
* Rerun run: /home/hectorluis.iglesias/projects/o3b/GW191216/GW191216-manual-run-O3b-C01-cleaned-60Hz-Lmax4-fmin-20-extrinsic_fixed_GPUs_neff_3_more_samples

### GW200129:
* Note: the rerun for this event was split into two separate runs (samples from both are used)

* Original run: /home/daniel.williams/events/O3/o3b/run_directories/S200129a/Prod7
* First rerun: /home/jacob.lange/unixhome/Projects/O3b-analysis/S200129a/S200129a-O3b-rerun-for-lnL-tail-my-samples-queue-15-neff-3-fixed-GPU-venv-SEOBNRv4PHM-Lmax4-fmin-20-extrinsic-2-first-half
* Second rerun: /home/jacob.lange/unixhome/Projects/O3b-analysis/S200129a/S200129a-O3b-rerun-for-lnL-tail-my-samples-queue-15-neff-3-fixed-GPU-venv-SEOBNRv4PHM-Lmax4-fmin-20-extrinsic-2-second-half

* options to check (i.e. to search for): d-max, srate, fmin-template (fstart), fmin-ifo, seglen=(data-end-time)-(data-start-time), channel-name, event-time, l-max