
#for i in {$1..$2}
for (( i=$1; i<=$2; ++i ))
do
    mkdir rundir_${i}
    mv CME_out-${i}-*.xml.gz rundir_${i}
    ligolw_add --ilwdchar-compat rundir_${i}/CME_out-${i}-*.xml.gz  --output CME_out-${i}-merged-new.xml.gz
#    echo "${i}"
#    find . -name "CME_out-${i}-*-0.xml_0_.xml.gz" | sed s/-0.xml_0_.xml.gz//g | sort | uniq > short_file.log
done
    
#    for i in `cat short_file.log`; do ligolw_add ${i}* --ilwdchar-compat --output ${i}_merged.xml.gz; done

